package cz.vse.java.xname00.adventuracv.main;

import cz.vse.java.xname00.adventuracv.logika.Hra;
import cz.vse.java.xname00.adventuracv.logika.IHra;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class AdventuraZaklad extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        BorderPane platno = new BorderPane();

        // konzole
        TextArea konzole = new TextArea();
        IHra hra = Hra.getSingleton();
        String uvitani = hra.vratUvitani() + "\n";
        konzole.setText(uvitani);
        platno.setTop(konzole);
        konzole.setEditable(false);

        Label popisek = new Label("Zadej příkaz:\t");
        popisek.setFont(Font.font("Arial", FontWeight.BLACK, 14.0));

        TextField uzivatelskyVstup = new TextField();

        HBox spodniBox = new HBox();
        spodniBox.setAlignment(Pos.CENTER);
        spodniBox.getChildren().addAll(popisek, uzivatelskyVstup);
        platno.setBottom(spodniBox);

        AnchorPane planekHry = new AnchorPane();
        Image planekHryImage = new Image(
                AdventuraZaklad.class.getResourceAsStream("/zdroje/herniPlan.png"));
        ImageView planekHryView = new ImageView(planekHryImage);
        Circle tecka = new Circle(20.0, Paint.valueOf("red"));
        planekHry.getChildren().addAll(planekHryView, tecka);

        tecka.setCenterX(100.0);
        tecka.setCenterY(300.0);

        AnchorPane.setLeftAnchor(tecka, 200.0);
        AnchorPane.setTopAnchor(tecka, 400.0);

        platno.setCenter(planekHry);

        uzivatelskyVstup.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String vstup = uzivatelskyVstup.getText();

                if (vstup.equals("teleportuj chaloupka")) {
                    AnchorPane.setLeftAnchor(tecka, 700.0);
                    AnchorPane.setTopAnchor(tecka, 200.0);
                } else {
                    String odpovedHry = hra.zpracujPrikaz(vstup);
                    konzole.appendText(String.format("\n%1$s\n", odpovedHry));
                }

                uzivatelskyVstup.clear();
            }
        });

        uzivatelskyVstup.requestFocus();
        Scene scene = new Scene(platno);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
